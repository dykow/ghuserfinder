const github = new GitHub;
const ui = new Ui;

// Seach input
const searchUser = document.getElementById('searchUser');

searchUser.addEventListener('keyup', (e) => {
  // Get input text
  const userText = e.target.value;

  if (userText !== '') {
    // Make http call
    github.getUser(userText)
      .then(data => {
        if (data.profile.message === 'Not Found') {
          // Show alert/error
          ui.showAlert('User not found', 'alert alert-danger');
        } else {
          // Clear remaining alert
          ui.clearAlert();
          // Show profile
          ui.showProfile(data.profile)
          ui.showRepos(data.repos);
        }
      })
  } else {
    // Clear the prfile
    ui.clearProfile();
    // Clear remaining alert
    ui.clearAlert();
  }
});
